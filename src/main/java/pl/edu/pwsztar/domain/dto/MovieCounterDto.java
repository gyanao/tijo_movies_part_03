package pl.edu.pwsztar.domain.dto;

public class MovieCounterDto {

    private Long counter;

    public MovieCounterDto(long counter) {
        this.counter = counter;
    }

    public long getCounter() {
        return counter;
    }

    public void setCounter(Long counter) {
        this.counter = counter;
    }

    @Override
    public String toString() {
        return "MovieCounterDto{" +
                "counter=" + counter +
                '}';
    }
}
